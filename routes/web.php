<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\PublishingHouseController;
use App\Models\Book;
use App\Models\Author;
use App\Models\PublishingHouse;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $books_count=Book::all()->count();
    $authors_count=Author::all()->count();
    $publishers_count=PublishingHouse::all()->count();
    return view('welcome', compact('books_count', 'authors_count', 'publishers_count'));
});

//Route::get('/', [IndexController::class, 'index']);

Route::resource('books', BookController::class);
Route::resource('authors', AuthorController::class);
Route::resource('publishers', PublishingHouseController::class);
