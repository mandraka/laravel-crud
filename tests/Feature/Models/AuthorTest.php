<?php

namespace Models;

use App\Models\Author;
use Tests\TestCase;

class AuthorTest extends TestCase
{
    public function testCreate()
    {
        $response = $this->json('POST', '/authors',[
            'first_name' => "Jan",
            'last_name' => "Kowalski"
        ]);
        $response->assertCreated();
    }
}
