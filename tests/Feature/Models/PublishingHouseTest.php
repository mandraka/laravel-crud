<?php

namespace Models;

use App\Models\PublishingHouse;
use Tests\TestCase;

class PublishingHouseTest extends TestCase
{
    public function testCreate()
    {
        $response = $this->json('POST', '/publishers',[
            'name' => "Book Publisher",
            'year_of_establishment' => "2005"
        ]);
        $response->assertCreated();
    }
}
