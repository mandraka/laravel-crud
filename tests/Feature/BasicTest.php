<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHome()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function testAuthors()
    {
        $response = $this->get('/authors');

        $response->assertStatus(200);
    }
    public function testPublishers()
    {
        $response = $this->get('/publishers');

        $response->assertStatus(200);
    }
    public function testBooks()
    {
        $response = $this->get('/books');

        $response->assertStatus(200);
    }
}
