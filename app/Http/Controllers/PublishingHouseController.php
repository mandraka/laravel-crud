<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PublishingHouse;

class PublishingHouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publisher = PublishingHouse::all();
        return view('publishers.index', compact('publisher'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('publishers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $storeData = $request->validate([
            'name' => 'required|max:255',
            'year_of_establishment' => 'required|numeric|min:1448|max:'.date("Y")
        ]);

        $publisher = PublishingHouse::create($storeData);

        return redirect('/publishers')->with('completed', 'Publishing house has been saved!')->setStatusCode(201);;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $publisher = PublishingHouse::findOrFail($id);
        return view('publishers.edit', compact('publisher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateData = $request->validate([
            'name' => 'required|max:255',
            'year_of_establishment' => 'required|numeric'
        ]);
        PublishingHouse::whereId($id)->update($updateData);
        return redirect('/publishers')->with('completed', 'Publishing house has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $publisher = PublishingHouse::findOrFail($id);
        try
        {
            $publisher->delete();
            return redirect('/publishers')->with('completed', 'Publishing house has been deleted');
        }
        catch (\Illuminate\Database\QueryException $e)
        {
            if ($e->errorInfo[0] == "23000") // SQLSTATE: Integrity constraint violation
            {
                return redirect('/publishers')->withErrors(["Can't remove publishing house when it has books signed to it."]);
            }
            else
            {
                return redirect('/publishers')->withErrors(["Publishing house deletion failed"]);
            }
       //
        }
    }
}
