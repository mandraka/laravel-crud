<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book = Book::all();
        return view('books/index', compact('book'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $storeData = $request->validate([
            'title' => 'required|max:255',
            'isbn' => 'required|min:13|max:17|regex:/[0-9-]/i',
            'year_of_publication' => 'required|numeric',
            'author_id' => 'required|numeric',
            'publisher_id' => 'required|numeric'
        ]);
        $storeData['isbn'] = str_replace("-","",$request['isbn']); // trim is not working with dashes
        $book = Book::create($storeData);

        return redirect('/books')->with('completed', 'Book has been saved!')->setStatusCode(201);;
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $isbn
     * @return \Illuminate\Http\Response
     */
    public function show($isbn)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $isbn
     * @return \Illuminate\Http\Response
     */
    public function edit($isbn)
    {
        $book = Book::findOrFail($isbn);
        return view('books/edit', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $isbn
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $isbn)
    {
        $updateData = $request->validate([
            'title' => 'required|max:255',
            'isbn' => 'required|min:13|max:17|regex:/[0-9-]/i',
            'year_of_publication' => 'required|numeric',
            'author_id' => 'required|numeric',
            'publisher_id' => 'required|numeric'
        ]);
        $storeData['isbn'] = str_replace("-","",$request['isbn']); // trim is not working with dashes
        Book::where('isbn', $isbn)->update($updateData);
        return redirect('/books')->with('completed', 'Book has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $isbn
     * @return \Illuminate\Http\Response
     */
    public function destroy($isbn)
    {
        $book = Book::where('isbn', $isbn)->firstOrFail();
        $book->delete();

        return redirect('/books')->with('completed', 'Book has been deleted');
    }
}
