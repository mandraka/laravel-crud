@extends('layout')

@section('content')

    <style>
        .container {
            max-width: 450px;
        }
        .push-top {
            margin-top: 50px;
        }
    </style>

    <div class="card push-top">
        <div class="card-header">
            Edit & Update
        </div>

        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('publishers.update', $publisher->id) }}">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" value="{{$publisher->name}}"/>
                </div>
                <div class="form-group">
                    <label for="year_of_establishment">Year of establishment</label>
                    <input type="number" class="form-control" name="year_of_establishment" value="{{$publisher->year_of_establishment}}"/>
                </div>
                <button type="submit" class="btn btn-block btn-danger">Update Publishing house</button>
            </form>
        </div>
    </div>
@endsection
