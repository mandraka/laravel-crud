@extends('layout')

@section('content')

    <style>
        .push-top {
            margin-top: 50px;
        }
    </style>

    <div class="push-top">
        @if(session()->get('completed'))
            <div class="alert alert-success">
                {{ session()->get('completed') }}
            </div><br />
        @endif
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
        <table class="table">
            <thead>
            <tr class="table-warning">
                <td>ID</td>
                <td>First name</td>
                <td>Last name</td>
                <td class="text-center">Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($author as $authors)
                <tr>
                    <td>{{$authors->id}}</td>
                    <td>{{$authors->first_name}}</td>
                    <td>{{$authors->last_name}}</td>
                    <td class="text-center">
                        <a href="{{ route('authors.edit', $authors->id)}}" class="btn btn-primary btn-sm">Edit</a>
                        <form action="{{ route('authors.destroy', $authors->id)}}" method="post" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm('Are you sure?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a href="{{ route('authors.create') }}" class="btn btn-primary btn-sm">Create</a>
    </div>
@endsection

