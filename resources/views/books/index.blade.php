@extends('layout')

@section('content')

    <style>
        .push-top {
            margin-top: 50px;
        }
    </style>

    <div class="push-top">
        @if(session()->get('completed'))
            <div class="alert alert-success">
                {{ session()->get('completed') }}
            </div><br />
        @endif
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
        <table class="table">
            <thead>
            <tr class="table-warning">
                <td>ISBN</td>
                <td>Title</td>
                <td>Year of publication</td>
                <td>Author</td>
                <td>Publishing house</td>
                <td class="text-center">Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($book as $books)
                <tr>
                    <td>{{$books->isbn}}</td>
                    <td>{{$books->title}}</td>
                    <td>{{$books->year_of_publication}}</td>
                    <td>{{$books->author_id}}</td>
                    <td>{{$books->publisher_id}}</td>
                    <td class="text-center">
                        <a href="{{ route('books.edit', $books->isbn)}}" class="btn btn-primary btn-sm">Edit</a>
                        <form action="{{ route('books.destroy', $books->isbn)}}" method="post" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm('Are you sure?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
            <a href="{{ route('books.create') }}" class="btn btn-primary btn-sm">Create</a>
        <div>
@endsection
