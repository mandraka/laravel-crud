@extends('layout')

@section('content')

    <style>
        .container {
            max-width: 450px;
        }
        .push-top {
            margin-top: 50px;
        }
    </style>

    <div class="card push-top">
        <div class="card-header">
            Edit & Update
        </div>

        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('books.update', $book->isbn) }}">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="title">Name</label>
                    <input type="text" class="form-control" name="title" value = "{{$book->title}}"/>
                </div>
                <div class="form-group">
                    <label for="isbn">ISBN</label>
                    <input type="text" class="form-control" name="isbn" value = "{{$book->isbn}}"/>
                </div>
                <div class="form-group">
                    <label for="year_of_publication">Year of publication</label>
                    <input type="number" class="form-control" name="year_of_publication" value = "{{$book->year_of_publication}}"/>
                </div>
                <div class="form-group">
                    <label for="author_id">Author</label>
                    <input type="number" class="form-control" name="author_id" value = "{{$book->author_id}}"/>
                </div>
                <div class="form-group">
                    <label for="publisher_id">Publishing house</label>
                    <input type="number" class="form-control" name="publisher_id" value = "{{$book->publisher_id}}"/>
                </div>
                <button type="submit" class="btn btn-block btn-danger">Update Book</button>
            </form>
        </div>
    </div>
@endsection
