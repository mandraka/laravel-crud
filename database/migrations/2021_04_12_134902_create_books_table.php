<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->char('isbn', 13)->primary();
			$table->string('title');
			$table->year('year_of_publication');
            $table->unsignedBigInteger('author_id');
            $table->unsignedBigInteger('publisher_id');
			$table->foreign('author_id')->references('id')->on('authors');
			$table->foreign('publisher_id')->references('id')->on('publishing_houses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
